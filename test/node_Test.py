import unittest
from node import Node

testNode1 = Node("asdf1", 0.23564)
testNode2 = Node("asdf2", 0.345724)

testNode3 = Node("asdf3", 0.3474)
testNode4 = Node("asdf3", 0.78967)
testNode5 = Node("asdf3", 0.34562)
testNode6 = Node("asdf3", 0.3467)

def start():
    testNode1.reset()
    testNode1.unwire()
    testNode2.reset()
    testNode2.unwire()
    testNode3.reset()
    testNode3.unwire()
    testNode4.reset()
    testNode4.unwire()
    testNode5.reset()
    testNode5.unwire()
    testNode6.reset()
    testNode6.unwire()

def testCanary():
    assert(True == True)

def testGetName():
    start()
    assert(testNode1.getName() == "asdf1")

def testHash():
    start()
    assert(hash(testNode1) == hash("asdf1"))

def testSettingValue():
    start()
    assert(testNode1.getValue() == 0.0)
    testNode1.setValue(0.77)
    assert(testNode1.getValue() == 0.77)

#def testSimpleChainActivated():
#    start()
#    testNode1.setValue(0.66)
#    testNode2.addInput(testNode1, 0.9)
#
#    # sigmoid(0.66 * 0.9) = 0.644282
#    testNode2.activate()
#    exval = 0.644282
#    recval = testNode2.getValue()
#    delta = 0.00001
#    assert(exval < recval + delta and exval > recval - delta)
#
#def testSimpleChainUnactivated():
#    start()
#    testNode1.setValue(0.66)
#    testNode3.addInput(testNode1, 0.9)
#
#    # sigmoid(0.66 * 0.9) = 0.644282
#    testNode3.activate()
#    exval = 0.0
#    recval = testNode3.getValue()
#    delta = 0.00001
#    assert(exval < recval + delta and exval > recval - delta)
#
#def testComplexChainActivated():
#    start()
#    testNode1.setValue(0.66)
#    testNode2.addInput(testNode1, 0.9)
#    testNode4.addInput(testNode2, 0.75)
#
#    # sigmoid(sigmoid(0.66 * 0.9) * 0.75) = 0.618506
#    testNode2.activate()
#    testNode4.activate()
#
#    exval = 0.25
#    recval = testNode4.getValue()
#    delta = 0.00001
#    assert(exval < recval + delta and exval > recval - delta)
#
#def testComplexChainUnactivated():
#    start()
#    testNode1.setValue(0.66)
#    testNode2.addInput(testNode1, 0.9)
#    testNode3.addInput(testNode2, 0.75)
#
#    # sigmoid(sigmoid(0.66 * 0.9) * 0.75) = 0.618506
#    testNode2.activate()
#    testNode3.activate()
#
#    exval = 0.0
#    recval = testNode4.getValue()
#    delta = 0.00001
#    assert(exval < recval + delta and exval > recval - delta)
#
#def testNetworkAcyclical():
#    start()
#    testNode1.setValue(0.66)
#    testNode2.setValue(0.33)
#
#    testNode3.addInput(testNode1, 0.9)
#    testNode3.addInput(testNode2, 0.75)
#
#    testNode4.addInput(testNode1, 0.23)
#    testNode4.addInput(testNode2, 0.52)
#
#    testNode5.addInput(testNode1, 0.12)
#    testNode5.addInput(testNode2, 0.87)
#
#    testNode6.addInput(testNode3, 0.78)
#    testNode6.addInput(testNode4, 0.81)
#    testNode6.addInput(testNode5, 0.43)
#
#    testNode3.activate() # sigmoid(0.66 * 0.90 + 0.33 * 0.75) + 0.10 = 0.798781 < 0.95
#    testNode4.activate() # sigmoid(0.66 * 0.23 + 0.33 * 0.52) + 0.10 = 0.680153 > 0.25
#    testNode5.activate() # sigmoid(0.66 * 0.12 + 0.33 * 0.87) + 0.23 = 0.820565 > 0.65
#
#    # sigmoid(0.0 * 0.78 + sigmoid(0.66 * 0.23 + 0.33 * 0.52) * 0.81 + sigmoid(0.66 * 0.12 + 0.33 * 0.87) * 0.43)
#    testNode6.activate()
#
#    exval = 0.55
#    recval = testNode6.getValue()
#    delta = 0.00001
#    assert(exval < recval + delta and exval > recval - delta)
#
#def testNetworkCyclical():
#    start()
#    testNode1.setValue(0.66)
#    testNode2.setValue(0.33)
#
#    testNode1.addInput(testNode6, 0.4)
#    testNode2.addInput(testNode6, 0.3)
#
#    testNode3.addInput(testNode1, 0.9)
#    testNode3.addInput(testNode2, 0.75)
#
#    testNode4.addInput(testNode1, 0.23)
#    testNode4.addInput(testNode2, 0.52)
#
#    testNode5.addInput(testNode1, 0.12)
#    testNode5.addInput(testNode2, 0.87)
#
#    testNode6.addInput(testNode3, 0.78)
#    testNode6.addInput(testNode4, 0.81)
#    testNode6.addInput(testNode5, 0.43)
#
#    testNode3.activate() # sigmoid(0.66 * 0.90 + 0.33 * 0.75) + 0.10 = 0.798781 < 0.95
#    testNode4.activate() # sigmoid(0.66 * 0.23 + 0.33 * 0.52) + 0.10 = 0.680153 > 0.25
#    testNode5.activate() # sigmoid(0.66 * 0.12 + 0.33 * 0.87) + 0.23 = 0.820565 > 0.65
#
#    # sigmoid(0.0 * 0.78 + sigmoid(0.66 * 0.23 + 0.33 * 0.52) * 0.81 + sigmoid(0.66 * 0.12 + 0.33 * 0.87) * 0.43)
#    testNode6.activate() # sigmoid(0.66 * 0.23 + 0.33 * 0.52)
#
#    testNode1.activate()
#    testNode2.activate()
#
#    testNode3.activate()
#    testNode4.activate()
#    testNode6.activate()
#
#    exval = 0.55
#    recval = testNode6.getValue()
#    print("\n\n*****\t\t" + str(testNode6.getValue()))
#    delta = 0.001
#    assert(exval < recval + delta and exval > recval - delta)
