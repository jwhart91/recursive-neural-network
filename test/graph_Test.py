import unittest
from graph import Graph
from node import Node

nodeU = Node('U', 0.1)
nodeV = Node('V', 0.1)
nodeW = Node('W', 0.1)
nodeX = Node('X', 0.1)
nodeY = Node('Y', 0.1)
nodeZ = Node('Z', 0.1)

deadNode1 = Node('$', 0.1)
deadNode2 = Node('%', 0.1)
deadNode3 = Node('!', 0.1)

vertices1 = {nodeU: [[nodeV, 0.1], [nodeW, 0.6]],
            nodeV: [[nodeW, 0.5], [nodeX, 0.5]],
            nodeW: [[nodeX, 0.3], [nodeY, 0.7]],
            nodeX: [[nodeW, 0.2]],
            nodeY: [[nodeZ, 0.7]],
            nodeZ: [[nodeW, 0.9]]}

vertices2 = {nodeU: [[nodeV, 0.1], [nodeW, 0.6]],
            nodeV: [[nodeW, 0.5], [nodeX, 0.5]],
            nodeW: [[nodeX, 0.3], [nodeV, 0.7]],
            nodeX: [[nodeY, 0.2]],
            nodeY: [[nodeZ, 0.7], [nodeX, 0.7]],
            nodeZ: [[nodeW, 0.9]],
            deadNode1: []}

vertices3 = {deadNode1 : []}

edges = [nodeU, nodeV, nodeW, nodeX, nodeY, nodeZ, deadNode1, deadNode2, deadNode3]
graph1 = Graph(edges, vertices1)
graph2 = Graph(edges, vertices2)
graph3 = Graph(edges, vertices3)


def testCanary():
    assert(True == True)

def testAddEdge():
    assert(deadNode2 not in vertices3)
    graph3.addEdge(deadNode2)
    assert(deadNode2 in vertices3)

def testAddEdge():
    graph3.addEdge(deadNode2)
    assert(deadNode3 not in vertices3[deadNode2])
    graph3.addVertex(deadNode2, deadNode3, 0.5)
    assert([deadNode3, 0.5] in vertices3[deadNode2])

def testFindPathOnSelf():
    path = graph1.findPath(nodeU, nodeU)
    assert(path == [nodeU])

def testFindPathBadStart():
    path = graph1.findPath(deadNode1, deadNode2)
    assert(path == [])

def testFindPathBadEnd():
    path = graph1.findPath(nodeU, deadNode2)
    assert(path == [])

def testFindPathSimple():
    path = graph1.findPath(nodeU, nodeV)
    expected = [nodeU, nodeV]
    assert(path == expected)

def testFindPathCycle():
    path = graph1.findPath(nodeY, nodeW)
    expected = [nodeY, nodeZ, nodeW]
    assert(path == expected)

def testFindPathLong():
    path = graph1.findPath(nodeU, nodeZ)
    expected = [nodeU, nodeV, nodeW, nodeY, nodeZ]
    assert(path == expected)

def testFindPathNoPath():
    path = graph1.findPath(nodeZ, nodeU)
    assert(path == [])

def testFindAllPathsBadStart():
    paths = graph1.findAllPaths(deadNode3, nodeU)
    assert(paths == [[]])

def testFindAllPathsOnSelf():
    paths = graph1.findAllPaths(nodeU, nodeU)
    expected = [[nodeU]]
    assert(paths == expected)

def testFindAllPathsSimple():
    paths = graph1.findAllPaths(nodeU, nodeW)
    expected = [[nodeU, nodeV, nodeW], [nodeU, nodeV, nodeX, nodeW], [nodeU, nodeW]]

    assert(paths == expected)

def testGetTopologicalOrder():
    paths = graph1.getTopologicalOrder([nodeU])
    expected =[nodeU, nodeV, nodeW, nodeX, nodeY, nodeZ]
    assert(paths == expected)

def testGetTopologicalOrderDegenerateCase():
    paths = graph2.getTopologicalOrder([deadNode1])
    expected = [deadNode1]
    assert(paths == expected)


def testGetTopologicalOrder2():
    paths = graph2.getTopologicalOrder([nodeX, nodeU])
    expected = [nodeX, nodeU, nodeY, nodeV, nodeZ, nodeW]
    assert(paths == expected)
