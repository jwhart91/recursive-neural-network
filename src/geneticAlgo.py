import random, sys, pickle, math, string
from network import Network
from node import Node
from graph import Graph

def sigmoid(numb):
    return 1.0 / (1.0 + math.exp(-1.0 * numb))

def createTimeSeries(length):
    series = [0] * length
    series[0] = random.random()
    for x in range(1, length):
        series[x] = series[x - 1] + ((2 * random.random()) - 1) * 0.05
        if series[x] < 0:
            series[x] = abs(series[x])
    return series

def estimateStdDev(inputs):
    avg = sum(inputs) / len(inputs)

    bsqrd = sum([(x ** 2 - avg ** 2) for x in inputs]) / (len(inputs))
    return math.sqrt(bsqrd)

def createNetworks(count):
    srcNetwork = Network.generateNewNetwork(10, 1, 1)
    topology = srcNetwork.topology.copy()
    innodes = srcNetwork.inputNodes.copy()
    outnodes = srcNetwork.outputNodes.copy()

    nets = []
    for x in range(0, count):
        newNet = Network(topology, innodes, outnodes)
        newNet.randomizeAllWeights()
        nets.append(newNet)

    return nets

def runNetworks(networks, inputs, outputs):
    for net in networks:
        net.resetNetwork()
        for datum in inputs:
            net.activateNetworkOneTimeStep([datum])

    top = sorted(networks, key = lambda n: n.getMeanSquareError(outputs))[0:int(len(networks) / 2)]

    return top

def runNetworksRecursive(networks, iterations):
    lastOutput = 0.0
    for net in networks:
        net.resetNetwork()
        for x in range(0, iterations):
            net.activateNetworkOneTimeStep([sigmoid(lastOutput)])
            lastOutput = net.getOutputValues()[0]

    top = sorted(networks, key = lambda n: n.getMeanSquareError([lastOutput]))[0:int(len(networks) / 2)]

    return top

def repopulate(existing, maxSize, factor):
    newPop = [t for t in existing]

    for x in range(0, maxSize):
        parent1 = existing[random.randint(0, len(existing) - 1)]
        parent2 = parent1
        while parent2 == parent1:
            parent2 = existing[random.randint(0, len(existing) - 1)]
        newNet = Network.averageNetworks(parent1, parent2)
        newPop.append(newNet)
        newNet.disturbAllWeights(factor)
        newPop.append(newNet)
    random.shuffle(newPop)

    return newPop[0:maxSize]

def runGeneticAlgorithmTimeSeries(generations):
    networks = createNetworks(10)
    for _ in range(0, generations):
        iterations = 251
        inputs = createTimeSeries(iterations)
        expected = [estimateStdDev(inputs)]

        top = runNetworks(networks, inputs, expected)
        print("Last best canidate error: " + str(top[0].getMeanSquareError(expected)))
        networks = repopulate(top, 10, 0.05)

def runGeneticAlgorithmReturnInput(generations):
    networks = createNetworks(10)
    for _ in range(0, generations):
        iterations = 25
        inputs = [random.random()] * iterations
        expected = [inputs[0]]

        top = runNetworks(networks, inputs, expected)
        print("Last best canidate error: " + str(top[0].getMeanSquareError(expected)))
        networks = repopulate(top, 10, 0.05)

def runGeneticAlgorithmSigmoidLastOutput(generations):
    networks = createNetworks(10)
    for _ in range(0, generations):
        iterations = 25

        top = runNetworksRecursive(networks, iterations)
        lastOutput = top[0].getOutputValues()[0]
        print("Last best canidate error: " + str(top[0].getMeanSquareError([sigmoid(lastOutput)])))
        networks = repopulate(top, 10, 0.15)

    top = runNetworksRecursive(networks, iterations)[0]
    lastOutput = top.getOutputValues()[0]
    top.activateNetworkOneTimeStep([sigmoid(lastOutput)])
    lastOutput = top.getOutputValues()[0]

runGeneticAlgorithmSigmoidLastOutput(10)











