from node import Node
import string
import random

class Graph:
    def __init__(self, edges, topology):
        self.edges = edges
        self.topology = topology
        return

    def addEdge(self, edge):
        if edge not in self.topology:
            self.topology.update({edge : []})
        return self.topology[edge]

    def addVertex(self, source, dest, weight):
        if source not in self.topology:
            self.addEdge(source)
        self.topology[source].append([dest, weight])
        return self.topology[source]

    # BFS implementation
    def findPath(self, source, dest, path = []):
        path = path + [source]
        if source == dest:
            return path

        if source not in self.topology:
            return []

        for node in self.topology[source]:
            if node[0] not in path:
                newpath = self.findPath(node[0], dest, path)
                if newpath != []:
                    return newpath
        return []


    def findAllPaths(self, source, dest, path = []):
        path = path + [source]
        if source == dest:
            return [path]

        if source not in self.topology:
            return [[]]

        paths = []
        for node in self.topology[source]:
            if node[0] not in path:
                newpaths = self.findAllPaths(node[0], dest, path)
                for newpath in newpaths:
                    paths.append(newpath)
        return paths

    def getTopologicalOrder(self, sources):
        def getForwardingSet(src):
            visited = []
            def rec(src):
                visited.append(src)
                for node in self.topology[src]:
                    if node[0] not in visited:
                        rec(node[0])
            rec(src)
            return visited

        orders = sorted([getForwardingSet(x) for x in sources], key = len)
        stack = []

        popped = True
        while popped:
            popped = False
            for y in sorted(orders, key = len):
                if(y != []):
                    val = y.pop(0)
                    if val not in stack:
                        stack.append(val)
                    popped = True

        return stack
