import pickle, sys, random, math, string
from graph import Graph
from node import Node

class Network:
    def __init__(self, topology, inputNodes, outputNodes):
        self.edges = [t for t in topology]
        self.topology = topology
        self.inputNodes = inputNodes
        self.outputNodes = outputNodes
        self.graph = Graph(self.edges, self.topology)
        self.schedule = self.graph.getTopologicalOrder(self.inputNodes)

        self.dummyInputNodes = []
        for inNode in inputNodes:
            dummy = Node(Network.nameGen(), 1.0)
            self.dummyInputNodes.append(dummy)
            inNode.addInput(dummy, 1.0)

        self.wireNetwork()
        self.lastError = 0.0
        self.desperation = 0.01

    def activateNetworkOneTimeStep(self, inputs):
        for x in range(len(self.dummyInputNodes)):
            self.dummyInputNodes[x].setValue(inputs[x])

        for node in self.schedule:
            node.activate()

        return [x.getValue() for x in self.outputNodes]

    def nameGen(size = 10):
        return ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(size))

    def generateRandomTopology(size):
        random.seed(random.SystemRandom().randint(-sys.maxsize, sys.maxsize))
        nodes = [Node(Network.nameGen(), random.random()) for _ in range(size)]

        topo = {}
        for x in nodes:
            topo.update({x : []})

        for x in nodes:
            for y in nodes:
                if x != y and random.random() > 0.55:
                    if topo[x] == []:
                        topo[x] = [[y, 1.0 - (random.random() * 0.1)]]
                    else:
                        topo[x].append([y, random.random()])
            if topo[x] == []:
                targ = x
                while targ == x:
                    targ = nodes[random.randint(0, size - 1)]
                topo[x] = [[targ, random.random()]]

        return topo

    def selectRandomNodes(topology, count):
        sampleSize = len(topology)
        selected = []
        nodes = [t for t in topology]
        while len(selected) < count:
            n = nodes[random.randint(0, sampleSize - 1)]
            if n not in selected:
                selected.append(n)
        return selected

    def generateNewNetwork(totalNodeCount, inputNodeCount, outputNodeCount):
        topo = Network.generateRandomTopology(totalNodeCount)
        inputNodes = Network.selectRandomNodes(topo, inputNodeCount)
        outputNodes = Network.selectRandomNodes(topo, outputNodeCount)

        return Network(topo, inputNodes, outputNodes)

    def wireNetwork(self):
        for node in self.topology:
            for targ in self.topology[node]:
                targ[0].addInput(node, targ[1])

    def resetNetwork(self):
        for node in self.topology:
            for targ in self.topology[node]:
                targ[0].reset()

    def getOutputValues(self):
        return [x.getValue() for x in self.outputNodes]

    def getMeanSquareError(self, expectedOutputs):
        subracand = []
        outputs = self.getOutputValues()

        for x in range(0, len(expectedOutputs)):
            subracand.append(expectedOutputs[x] - outputs[x])

        return (sum([x ** 2 for x in subracand]) / (len(subracand) + 1))

    def addNodeAtRandom(self):
        newNode = Node(Network.nameGen(), random.random())
        self.topology.update({newNode: []})

        for n in Network.selectRandomNodes(self.topology, 2):
            weight = random.random()
            self.topology[n].append([newNode, weight])
            newNode.addInput(n, weight)

        for n in Network.selectRandomNodes(self.topology, 2):
            weight = random.random()
            self.topology[newNode].append([n, weight])
            n.addInput(newNode, weight)

    def removeNodeAtRandom(self):
        doomedNode = Network.selectRandomNodes(self.topology, 1)[0]
        for node in self.topology:
            for links in self.topology[node]:
                if links[0] == doomedNode:
                    del links
            for inp in node.inputs:
                if inp[0] == doomedNode:
                    del inp

        self.topology.pop(doomedNode, None)

    def combineNetworks(net1, net2):
        newTopo = net1.topology.copy()

        for edge in newTopo:
            if random.random() > 0.50:
                newTopo[edge] = net2.topology[edge]

        return Network(newTopo, net1.inputNodes, net1.outputNodes)

    def averageNetworks(net1, net2):
        newTopo = net1.topology.copy()

        for edge in newTopo:
            if random.random() > 0.50:
                for x in range(0, len(newTopo[edge]) - 1):
                    newTopo[edge][x][1] = (newTopo[edge][x][1] + net2.topology[edge][x][1]) / 2

        return Network(newTopo, net1.inputNodes, net1.outputNodes)

    def randomizeAllWeights(self):
        for edge in self.topology:
            for links in self.topology[edge]:
                links[1] = random.random()

    def disturbAllWeights(self, factor):
        for edge in self.topology:
            for links in self.topology[edge]:
                links[1] = links[1] + factor * ((2 * random.random()) - 1)


