from math import exp

class Node:
    def __init__(self, name, activation):
        self.name = name
        self.activation = activation
        self.value = 0.0
        self.inputs = []

    def getName(self):
        return self.name

    def getValue(self):
        return self.value

    def setValue(self, value):
        self.value = value

    def addInput(self, node, weight):
        self.inputs.append([node, weight])

    def reset(self):
        self.value = 0.0

    def unwire(self):
        self.inputs = []

    def activate(self):
        value = self.sigmoid(sum([inp[0].getValue() * inp[1] for inp in self.inputs]))
        self.value = self.activation * value

    def sigmoid(self, numb):
        return 1.0 / (1.0 + exp(-1.0 * numb))

    def __hash__(self):
        return hash(self.name)
