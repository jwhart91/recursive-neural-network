import unittest
from network import Network

def printDictionary(dictionary):
    print("{")
    for k in dictionary:
        name = k.getName()
        data = "{"
        for l in dictionary[k]:
            data = data + l[0].getName() + ", "
        data = "\t" + name + " : " + data + "}"
        print(data)
    print("}")

def testRandomGenerator():
    assert(10 == len(Network.generateRandomTopology(10)))

def testSelectRandomNodes():
    topology = Network.generateRandomTopology(10)
    selected = Network.selectRandomNodes(topology, 3)
    assert(3 == len(selected))

def testNetworkCreation():
    topology = Network.generateRandomTopology(10)
    inNodes = Network.selectRandomNodes(topology, 3)
    outNodes = Network.selectRandomNodes(topology, 2)

    n = Network(topology, inNodes, outNodes)
    assert(1 == 1)

def testNetworkCreationLarge():
    topology = Network.generateRandomTopology(100)
    inNodes = Network.selectRandomNodes(topology, 30)
    outNodes = Network.selectRandomNodes(topology, 20)

    n = Network(topology, inNodes, outNodes)
    assert(1 == 1)

def testNetworkFiring():
    topology = Network.generateRandomTopology(10)
    inNodes = Network.selectRandomNodes(topology, 3)
    outNodes = Network.selectRandomNodes(topology, 2)

    n = Network(topology, inNodes, outNodes)
    outputs = n.activateNetworkOneTimeStep([0.5, 0.9, 0.9])
    assert(2 == len(outputs))

def testNetworkFiringIteration():
    n = Network.generateNewNetwork(10, 3, 2)

    for x in range(20):
        n.activateNetworkOneTimeStep([0.5, 0.9, 0.9])
        outputs = n.getOutputValues()

    n.resetNetwork()
    assert(2 == len(outputs))


def testNetworkAddNode():
    n = Network.generateNewNetwork(10, 3, 2)

    for x in range(20):
        n.activateNetworkOneTimeStep([0.5 * x, 0.9 / (x + 1), 0.3 / (x + 1)])
        n.addNodeAtRandom()
        outputs = n.getOutputValues()
    assert(30 == len(n.topology))


def testNetworkRemoveNode():
    n = Network.generateNewNetwork(30, 3, 2)

    for x in range(20):
        n.activateNetworkOneTimeStep([0.5 * x, 0.9 / (x + 1), 0.3 / (x + 1)])
        n.removeNodeAtRandom()
        outputs = n.getOutputValues()
    assert(10 == len(n.topology))

def testNetworkCombine():
    n = Network.generateNewNetwork(10, 3, 2)
    top = n.topology.copy()
    inset = n.inputNodes.copy()
    outset = n.outputNodes.copy()
    n2 = Network(top, inset, outset)

    combinedNet = Network.combineNetworks(n, n2)
    for x in range(20):
        combinedNet.activateNetworkOneTimeStep([0.5 * x, 0.9 / (x + 1), 0.3 / (x + 1)])
        outputs = combinedNet.getOutputValues()

    assert(10 == len(combinedNet.topology))

def testNetworkAverage():
    n = Network.generateNewNetwork(10, 3, 2)
    top = n.topology.copy()
    inset = n.inputNodes.copy()
    outset = n.outputNodes.copy()
    n2 = Network(top, inset, outset)

    combinedNet = Network.averageNetworks(n, n2)
    for x in range(20):
        combinedNet.activateNetworkOneTimeStep([0.5 * x, 0.9 / (x + 1), 0.3 / (x + 1)])
        outputs = combinedNet.getOutputValues()
    assert(10 == len(combinedNet.topology))

def testRandomizeWeights():
    n = Network.generateNewNetwork(10, 3, 2)
    n.randomizeAllWeights()

    assert(10 == len(n.topology))
